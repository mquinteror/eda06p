all:
	pdflatex -shell-escape reporte.tex
	figlet "ya puedes abrir el PDF "
	nohup evince reporte.pdf &
	rm *.toc *.aux *.out *.log *.py

